const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: __dirname + "/index.js",
    output: {
        path: __dirname + "/build/assets",
        filename: "bundle.js"
    },
    performance: { hints: false },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['react']
                        }
                    }
                ],
            },
            {
                test: /\.html$/,
                use: [{
                    loader: 'html-loader',
                    options: {
                        minimize: true,
                        removeComments: false,
                        collapseWhitespace: false
                    }
                }],
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx'],
    },
    plugins: [
        new HtmlWebPackPlugin({
            inject: true,
            template: "./public/index.html",
            filename: "index.html"
        })
    ]
};