import React from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom";
import PageRoute from "./Components/Page";

const App = () => (
    <Router>
        <PageRoute/>
    </Router>
);

export default App;