import {Route} from "react-router-dom";
import Page from "./Layout";
import React from "react";

const PageRoute = () => (
    <div>
        <Route path="/" exact component={Page}/>
    </div>
);

export default PageRoute;