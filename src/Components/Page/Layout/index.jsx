import React, {Component} from 'react';
import Canvas from "../../Canvas";  //here panel with beetle
import ControlPanel from "../../../Modules/ControlPanel";
import PropTypes from "prop-types";

class Page extends Component {

    constructor(props) {
        super(props);

        this.state = {
            flag: false,  // flag for direction beetle
            flagTeleport: false,
            speed: 1
        };

        this.handleFlag = this.handleFlag.bind(this);
        this.handleFlagTeleport = this.handleFlagTeleport.bind(this);
        this.handleSpeed = this.handleSpeed.bind(this);
    }

    handleFlag() {
        const {flag} = this.state;
        this.setState({flag: !flag});
    }

    handleFlagTeleport() {
        const {flagTeleport} = this.state;
        this.setState({flagTeleport: !flagTeleport});
    }

    handleSpeed(e) {
        this.setState({speed: parseInt(e.target.value)});
    }

    render() {
        const {flag, speed, flagTeleport} = this.state;
        return (
            <div>
                <ControlPanel
                    flagTeleport={flagTeleport}
                    handleFlagTeleport={this.handleFlagTeleport}
                    flag={flag}
                    handleFlag={this.handleFlag}
                    handleSpeed={this.handleSpeed}
                    speed={!flagTeleport ? speed : 40}
                />
                <Canvas
                    flag={flag}
                    speed={!flagTeleport ? speed : 40}
                />
            </div>
        )
    }
}

export default Page;