import React, {Component} from 'react';
import {Stage, Layer} from 'react-konva';
import Beetle from "../../Modules/Beetle";  //layout beetle
import PropTypes from "prop-types";

class Canvas extends Component {
    constructor(...args) {
        super(...args);
        this.state = {
            cursor: {
                x: 0,
                y: 0
            },
            beetle: {
                x: 200,
                y: 200,
            },
        };
        this.changeBeetlePosition = this.changeBeetlePosition.bind(this);
        this.getCursorPosition = this.getCursorPosition.bind(this);
        this.handleMouseMoveToCursor = this.handleMouseMoveToCursor.bind(this);
        this.handleMouseMoveFromCursor = this.handleMouseMoveFromCursor.bind(this);
    }

    changeBeetlePosition(beetleCopy) {
        this.setState({beetle: beetleCopy})  //template set state of beetle
    };

    getCursorPosition(stage) {
        this.setState({
            cursor: stage.getPointerPosition(),  //get cursor position
        });
    };

    /*
    * move TO cursor logic
    * */
    handleMouseMoveToCursor(e) {
        const {beetle, cursor} = this.state;
        let stage = e.target.getStage();
        const {speed} = this.props;
        const beetleCopy = Object.assign({}, beetle);
        this.getCursorPosition(stage);

        if ((beetle.x !== cursor.x) && (beetle.y !== cursor.y)) {
            if ((((beetle.y + 35) - cursor.y) < 0) && cursor.x) {
                beetleCopy.y = beetleCopy.y + speed;
                this.changeBeetlePosition(beetleCopy)
            }
            if ((((beetle.y - 35) - cursor.y) > 0) && cursor.x) {
                beetleCopy.y = beetleCopy.y - speed;
                this.changeBeetlePosition(beetleCopy)
            }
            if ((((beetle.x + 35) - cursor.x) < 0) && cursor.y) {
                beetleCopy.x = beetleCopy.x + speed;
                this.changeBeetlePosition(beetleCopy)
            }
            if ((((beetle.x - 35) - cursor.x) > 0) && cursor.y) {
                beetleCopy.x = beetleCopy.x - speed;
                this.changeBeetlePosition(beetleCopy)
            }
        } else return null
    };

    /*
    * move FROM cursor logic
    * */
    handleMouseMoveFromCursor(e) {
        const {cursor, beetle} = this.state;
        const {speed} = this.props;
        const indent = 70;  //this radius for cursor
        const actionRadius = 100;
        let stage = e.target.getStage();
        let beetleCopy = Object.assign({}, beetle);
        let beetleCopyForIf = Object.assign({}, beetle);
        this.getCursorPosition(stage);

        /*
        *page boundary condition
        * */
        if (beetleCopy.x <= indent) {
            beetleCopy.x = indent;
            this.changeBeetlePosition(beetleCopy)
        } else if (beetleCopy.x >= window.innerWidth - indent) {
            beetleCopy.x = window.innerWidth - indent;
            this.changeBeetlePosition(beetleCopy)
        }
        if (beetleCopy.y <= indent) {
            beetleCopy.y = indent;
            this.changeBeetlePosition(beetleCopy)
        } else if (beetleCopy.y >= 500 - indent) {
            beetleCopy.y = 500 - indent;
            this.changeBeetlePosition(beetleCopy)
        }

        /*
        * distance move circle
        * */
        if (
            beetleCopy.x - cursor.x <= actionRadius &&
            beetleCopy.x - cursor.x >= -actionRadius &&
            beetleCopy.y - cursor.y <= actionRadius &&
            beetleCopy.y - cursor.y >= -actionRadius
        ) {
            /*
            *move beetle
            **/
            if ((((beetleCopyForIf.y + 35) - cursor.y) < 0) && cursor.x) {
                beetleCopy.y = beetleCopy.y - speed;
                this.changeBeetlePosition(beetleCopy)
            }
            if ((((beetleCopyForIf.y - 35) - cursor.y) > 0) && cursor.x) {
                beetleCopy.y = beetleCopy.y + speed;
                this.changeBeetlePosition(beetleCopy)
            }
            if ((((beetleCopyForIf.x - 35) - cursor.x) < 0) && cursor.y) {
                beetleCopy.x = beetleCopy.x - speed;
                this.changeBeetlePosition(beetleCopy)
            }
            if ((((beetleCopyForIf.x + 35) - cursor.x) > 0) && cursor.y) {
                beetleCopy.x = beetleCopy.x + speed;
                this.changeBeetlePosition(beetleCopy)
            }
        }
    };

    render() {
        const {beetle} = this.state;
        const {flag} = this.props;
        const action = flag ? this.handleMouseMoveToCursor : this.handleMouseMoveFromCursor;
        return (
            <Stage
                width={window.innerWidth}
                height={500}
                onMouseMove={action}
                strokeWidth={2}
                style={{border: '1px solid'}}
            >
                <Layer>
                    <Beetle
                        x={beetle.x}
                        y={beetle.y}
                    />
                </Layer>
            </Stage>
        );
    }
}

Canvas.propTypes = {
    flag: PropTypes.bool.isRequired,
    speed: PropTypes.number.isRequired
};

export default Canvas;