import React from 'react';
import Speed from "../Speed";
import Move from "../Move";
import Title from "../Title";
import PropTypes from "prop-types";

const ControlPanel = (props) => (
    <div>
        <Title flagTeleport={props.flagTeleport}/>
        <button onClick={() => props.handleFlagTeleport()}>
            Change line move
        </button>
        <Move
            flag={props.flag}
            handleFlag={props.handleFlag}
        />
        {
            !props.flagTeleport &&
            <Speed
                speed={props.speed}
                handleSpeed={props.handleSpeed}
            />
        }
    </div>
);

ControlPanel.propTypes = {
    speed: PropTypes.number.isRequired,
    handleSpeed: PropTypes.func.isRequired,
    handleFlagTeleport: PropTypes.func.isRequired,
    flagTeleport: PropTypes.bool.isRequired,
    flag: PropTypes.bool.isRequired,
};

export default ControlPanel;