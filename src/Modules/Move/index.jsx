import React,{Component} from "react";
import PropTypes from "prop-types";

class Move extends Component{
    render() {
        const {flag, handleFlag} = this.props;
        return(
            <div>
                <strong>Direction beetle:</strong>
                <button onClick={() => handleFlag()}>
                    {flag ? 'TO' : 'FROM'} CURSOR
                </button>
            </div>
        )
    }
}

Move.propTypes = {
    flag: PropTypes.bool.isRequired,
    handleFlag: PropTypes.func.isRequired
};

export default Move;