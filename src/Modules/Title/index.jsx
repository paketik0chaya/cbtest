import React from 'react';

const Title = (props) => (
    <h3>
        {
            props.flagTeleport ?
                'ANTI-LINE move'
                :
                'LINE move'
        }
    </h3>
);

export default Title;