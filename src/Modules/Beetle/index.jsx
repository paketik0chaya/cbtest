import React, {Component} from 'react';
import {Circle} from "react-konva";
import PropTypes from 'prop-types';

class Beetle extends Component {

    render() {
        const {x, y} = this.props;
        return (
            <Circle
                dragDistance={15}
                x={x}
                y={y}
                radius={30}
                fill={"green"}
                shadowBlur={5}
            />
        )
    }
}

Beetle.propTypes = {
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired
};

export default Beetle;