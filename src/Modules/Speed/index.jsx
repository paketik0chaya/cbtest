import React, {Component} from "react";
import PropTypes from "prop-types";

class Speed extends Component {
    render() {
        const {speed, handleSpeed} = this.props;
        return (
            <div>
                <strong>Speed beetle:</strong>
                <input type="number" name="speed" min="1" max="100" value={speed} onChange={(e) => handleSpeed(e)}/>
            </div>
        );
    }
}

Speed.propTypes = {
    speed: PropTypes.number.isRequired,
    handleSpeed: PropTypes.func.isRequired
};

export default Speed;